DELETE  historial;

CREATE TABLE historial (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  usuario VARCHAR(25) NOT NULL,
  puntuacion INT NOT NULL,
  imdbID VARCHAR(15) DEFAULT NULL
);