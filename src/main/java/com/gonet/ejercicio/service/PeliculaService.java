package com.gonet.ejercicio.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import com.gonet.ejercicio.dto.PeliculaDTO;

@Service
public class PeliculaService {

	@Autowired
	private RestTemplate restTemplate;
	
	
	
	public PeliculaDTO getPeliculaByName(String name)
	{
	    final String uri = "http://www.omdbapi.com/?apikey=b508f41e&t="+name;
	    PeliculaDTO result = restTemplate.getForObject(uri, PeliculaDTO.class);
	    System.out.println(result);
	    return result;
	}



	public PeliculaDTO getPeliculaById(String imdbID) {
		final String uri = "http://www.omdbapi.com/?apikey=b508f41e&i="+imdbID;
	    PeliculaDTO result = restTemplate.getForObject(uri, PeliculaDTO.class);
	    System.out.println(result);
	    return result;
	}



	
}
