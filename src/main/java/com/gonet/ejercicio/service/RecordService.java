package com.gonet.ejercicio.service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.gonet.ejercicio.dto.PeliculaDTO;
import com.gonet.ejercicio.dto.RecordDTO;
import com.gonet.ejercicio.dto.RecordResponseDTO;
import com.gonet.ejercicio.model.RecordMovie;
import com.gonet.ejercicio.repositories.RecordRepository;

@Service
public class RecordService {

	@Autowired
	private RecordRepository recordRepository;

	
	@Autowired
	PeliculaService peliculaService;
	
	
	public void save(RecordDTO recordDTO) {
		RecordMovie recordMovie= new RecordMovie ();
		recordMovie.setImdbID(recordDTO.getImdbID());
		recordMovie.setPuntuacion(recordDTO.getPuntuacion());
		recordMovie.setUsuario(recordDTO.getUsuario());
		recordRepository.save(recordMovie);
		
	}

	public List<RecordResponseDTO> getHistorial(String usuario) {
		List<RecordMovie> records= recordRepository.findByUsuario(usuario);
		List<RecordResponseDTO> responseList= new ArrayList<>();
		
		for(RecordMovie eecordMovie : records ) {
			RecordResponseDTO recordResponseDTO= new RecordResponseDTO();
			RecordDTO recordDTO= new RecordDTO();
			recordDTO.setPuntuacion(eecordMovie.getPuntuacion());
			recordDTO.setImdbID(eecordMovie.getImdbID());
			recordResponseDTO.setRecord(recordDTO);
			
			PeliculaDTO peliculaDTO = peliculaService.getPeliculaById(eecordMovie.getImdbID());
			recordResponseDTO.setPelicula(peliculaDTO);
			
			responseList.add(recordResponseDTO);
		}
	
		return responseList;
	}

	public void update(RecordDTO recordDTO) {
		
	RecordMovie	recordMovie= recordRepository.findByUsuarioAndImdbID(recordDTO.getUsuario(),recordDTO.getImdbID());
		recordMovie.setImdbID(recordDTO.getImdbID());
		recordMovie.setPuntuacion(recordDTO.getPuntuacion());
	recordRepository.save(recordMovie);
	}

	public void delete(RecordDTO recordDTO) {
		RecordMovie	recordMovie= recordRepository.findByUsuarioAndImdbID(recordDTO.getUsuario(),recordDTO.getImdbID());
		recordRepository.delete(recordMovie);
		
	}
	
	
	
	
	
	
}
