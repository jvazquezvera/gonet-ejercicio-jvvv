package com.gonet.ejercicio.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import com.gonet.ejercicio.model.RecordMovie;


public interface RecordRepository extends CrudRepository<RecordMovie, Integer> {

     List<RecordMovie> findByUsuario(String usuario);
     
     RecordMovie findByUsuarioAndImdbID(String usuario, String imdbID);
     
	
}
