package com.gonet.ejercicio.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class RecordDTO {
	
	
    @NotNull(message = "La puntuacion es requerida")
    @Min(value = 0, message = "La calificacion minima es 0")
    @Max(value = 10, message = "La puntuacion maxima es 10")
    private Integer puntuacion;
    @NotNull(message = "El id de la pelicula es requerido")
	private String imdbID;
    
    
    private String usuario;
    
	public Integer getPuntuacion() {
		return puntuacion;
	}
	public void setPuntuacion(Integer puntuacion) {
		this.puntuacion = puntuacion;
	}
	public String getImdbID() {
		return imdbID;
	}
	public void setImdbID(String imdbID) {
		this.imdbID = imdbID;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	
	
}
