package com.gonet.ejercicio.dto;

public class RecordResponseDTO {

	RecordDTO record;
	PeliculaDTO pelicula;
	public RecordDTO getRecord() {
		return record;
	}
	public void setRecord(RecordDTO record) {
		this.record = record;
	}
	public PeliculaDTO getPelicula() {
		return pelicula;
	}
	public void setPelicula(PeliculaDTO pelicula) {
		this.pelicula = pelicula;
	}
	
	
	
}
