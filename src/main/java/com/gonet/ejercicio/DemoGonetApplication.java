package com.gonet.ejercicio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoGonetApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoGonetApplication.class, args);
	}
}
