package com.gonet.ejercicio.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.gonet.ejercicio.dto.PeliculaDTO;
import com.gonet.ejercicio.service.PeliculaService;




@RestController
@RequestMapping("/movie")
public class PeliculaController {

	@Autowired
	PeliculaService peliculaService;
	


	@GetMapping("/findByName/{nombrePelicula}")
	public PeliculaDTO getTreeEmployee(Authentication authentication,
			@PathVariable String nombrePelicula
			) {  
			System.out.println("authentication"+authentication.getName());
		return peliculaService.getPeliculaByName(nombrePelicula);  
	}
	
	
}
