package com.gonet.ejercicio.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.gonet.ejercicio.dto.RecordDTO;
import com.gonet.ejercicio.dto.RecordResponseDTO;
import com.gonet.ejercicio.service.RecordService;




@RestController
@RequestMapping("/movie/record")
public class PeliculaHistorialController {

	@Autowired
	RecordService recordService;
	
	 @PutMapping
	  public void addRegistro(Authentication authentication, @RequestBody RecordDTO recordDTO) {
		 recordDTO.setUsuario(authentication.getName());
		 recordService.save(recordDTO);
	  }
	 
	 @GetMapping
	   public List<RecordResponseDTO> getHistorial(Authentication authentication) {  
			return recordService.getHistorial(authentication.getName());  
		}
	
	 
	 //todo
	 @PostMapping
	  public void updateRegistro(Authentication authentication, @RequestBody RecordDTO recordDTO) {
		 recordDTO.setUsuario(authentication.getName());
		 recordService.update(recordDTO);
	  }
	 
	 
	 @DeleteMapping
	  public void deleteRegistro(Authentication authentication, @RequestBody RecordDTO recordDTO) {
		 recordDTO.setUsuario(authentication.getName());
		 recordService.delete(recordDTO);
	  }
	 
}
