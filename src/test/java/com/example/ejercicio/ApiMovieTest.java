package com.example.ejercicio;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.oauth2.common.util.JacksonJsonParser;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;

import com.gonet.ejercicio.DemoGonetApplication;



@RunWith(SpringRunner.class)
@WebAppConfiguration
@SpringBootTest(classes = DemoGonetApplication.class)
@Transactional
public class ApiMovieTest {

	
	 	@Autowired
	    private WebApplicationContext wac;
	 
	    @Autowired
	    private FilterChainProxy springSecurityFilterChain;
	 
	    private MockMvc mockMvc;
	 
	    @Before
	    public void setup() {
	        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac)
	          .addFilter(springSecurityFilterChain).build();
	    }
	
	
	    private String obtainAccessToken(String username, String password) throws Exception {
	    	 
	        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
	        params.add("grant_type", "password");
	        params.add("client_id", "client");
	        params.add("username", username);
	        params.add("password", password);
	     
	        ResultActions result 
	          = mockMvc.perform(post("/oauth/token")
	            .params(params)
	            .with(httpBasic("client","clientpassword"))
	            .accept("application/json;charset=UTF-8"))
	            .andExpect(status().isOk())
	            .andExpect(content().contentType("application/json;charset=UTF-8"));
	     
	        String resultString = result.andReturn().getResponse().getContentAsString();
	     
	        JacksonJsonParser jsonParser = new JacksonJsonParser();
	        return jsonParser.parseMap(resultString).get("access_token").toString();
	    }
	    
	    
	@Test
	public void pruebaBuscarPelicula() {
		try {
			String accessToken = obtainAccessToken("user2", "user2");
			mockMvc.perform(get("/movie/findByName/start")
				      .header("Authorization", "Bearer " + accessToken)
				      .accept("application/json;charset=UTF-8"))
				      .andExpect(status().isOk()).andDo(print());
		
		} catch (Exception e) {
			Assert.fail("Exception " + e);
		}
	}

	
	@Test
	public void pruebaHistorialGet(){
		try {
			String accessToken = obtainAccessToken("user2", "user2");
			mockMvc.perform(get("/movie/record")
				      .header("Authorization", "Bearer " + accessToken)
				      .accept("application/json;charset=UTF-8"))
				      .andExpect(status().isOk()).andDo(print());
		
		} catch (Exception e) {
			Assert.fail("Exception " + e);
		}
	}
	
	@Test
	public void pruebaHistorialPut(){
		try {
			String accessToken = obtainAccessToken("user2", "user2");
			mockMvc.perform(put("/movie/record")
				      .header("Authorization", "Bearer " + accessToken)
				      .contentType(MediaType.APPLICATION_JSON_VALUE)
                      .accept(MediaType.APPLICATION_JSON)	
				      .content("{\"puntuacion\":\"12\",\"imdbID\":\"tt0076759\" }"))
			          .andExpect(status().isOk()).andDo(print());
		
		} catch (Exception e) {
			Assert.fail("Exception " + e);
		}
	}
	
	@Test
	public void pruebaHistorialPostDelete(){
		try {
			String accessToken = obtainAccessToken("user2", "user2");
			
			mockMvc.perform(put("/movie/record")
				      .header("Authorization", "Bearer " + accessToken)
				      .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .accept(MediaType.APPLICATION_JSON)	
				      .content("{\"puntuacion\":\"12\",\"imdbID\":\"tt0076759\" }"))
			          .andExpect(status().isOk()).andDo(print());
			
			mockMvc.perform(post("/movie/record")
				      .header("Authorization", "Bearer " + accessToken)
				      .contentType(MediaType.APPLICATION_JSON_VALUE)
                      .accept(MediaType.APPLICATION_JSON)	
				      .content("{\"puntuacion\":\"12\",\"imdbID\":\"tt0076759\" }"))
			          .andExpect(status().isOk());
			
			mockMvc.perform(delete("/movie/record")
				      .header("Authorization", "Bearer " + accessToken)
				      .contentType(MediaType.APPLICATION_JSON_VALUE)
                      .accept(MediaType.APPLICATION_JSON)	
				      .content("{\"puntuacion\":\"12\",\"imdbID\":\"tt0076759\" }"))
			          .andExpect(status().isOk());
			
		
		} catch (Exception e) {
			Assert.fail("Exception " + e);
		}
	}
	
	
	
}
